-- Hide Status Bar
display.setStatusBar(display.HiddenStatusBar);

-- Physics Engine
local physics = require "physics";
physics.start();
physics.setGravity(0, 4);

-- Load Sounds
local bgMusic = audio.loadStream("MrJellyRolls.mp3")


-- "Constants"
local _W = display.contentWidth / 2;
local _H = display.contentHeight / 2;

-- Variables

-- Menu Screen
local titleScreenGroup;
local titleScreen;
local playBtn;
 

 
-- textBoxGroup
local textBoxGroup;
local textBox;
local conditionDisplay;
local messageText;

-- Main Function
function main()
  showTitleScreen();
end

-- Show the Title Screen
function showTitleScreen()  
 
  -- Place all title screen elements into 1 group
  titleScreenGroup = display.newGroup();
 
  -- Display background image
  titleScreen = display.newImage("start.png", display.contentWidth, display.contentHeight);
  titleScreen.x = _W;
  titleScreen.y = _H;
 
  -- Display play button image
  playBtn = display.newImage("play.png");
  playBtn.x = _W;
  playBtn.y = _H + 50;
  playBtn.name = "playbutton";
 
  -- Insert background and button into group
  titleScreenGroup:insert(titleScreen);
  titleScreenGroup:insert(playBtn);

  -- Make play button interactive
  playBtn:addEventListener("tap", loadGame);
end

-- When play button is tapped, start the game
function loadGame(event)
  if event.target.name == "playbutton" then
    audio.play(bgMusic, {loops=-1});
    audio.setVolume(0.2)
    transition.to(titleScreenGroup,{time = 0, alpha=0, onComplete = startGame});
    playBtn:removeEventListener("tap", loadGame);
  end
end

function startGame()
  local physics = require("physics")
  physics.start()
end

-- Run the game
main();

-- When the game starts, add physics properties
function startGame()
  audio.play( bgMusic )
local background = display.newImageRect("bkg.png" , display.contentWidth, display.contentHeight)
background.x = display.contentCenterX
background.y = display.contentCenterY 

local throwSound = audio.loadSound( "throw.mp3" )

local loselifeSound = audio.loadSound( "loselife.mp3" )

local addlifeSound = audio.loadSound( "addlife.wav" )

local gameoverSound = audio.loadSound( "gameovermusic.mp3" )

local Lives= 5 

local LivesText 

local LivesTxt= display.newText("Lives : 5", 130 , 30 ,"Arial",60)

local Scores= 0 

local ScoresText 

local ScoresTxt= display.newText("Scores: 0", 920 , 30 ,"Arial",60)


display.setStatusBar( display.HiddenStatusBar )
local physics = require( "physics" )
physics.start()
local _W = display.contentWidth
local _H = display.contentHeight
display.setDefault( "cameraSource", "back" )
local ground = display.newRect(display.contentCenterX,display.contentHeight, display.contentWidth, display.contentHeight*.01)
ground:setFillColor(0,0,0)
ground.name = "ground"
physics.addBody(ground, "static")


local roof = display.newRect(display.contentCenterX, 0, display.contentWidth, display.contentHeight*.01)
roof:setFillColor(0,0,0)
roof.name = "roof"

local rightwall = display.newRect(0, display.contentCenterY, display.contentWidth*.02, display.contentHeight)
rightwall:setFillColor(0,0,0)
rightwall.name = "right wall"

local leftwall = display.newRect(display.contentWidth, display.contentCenterY, display.contentWidth*.02, display.contentHeight)
leftwall:setFillColor(0,0,0)
leftwall.name = "left wall"
physics.addBody(roof, "static")
physics.addBody(rightwall, "static")
physics.addBody(leftwall, "static")
physics.addBody( ground,"static" )
local hole = display.newCircle(display.contentCenterX, display.contentCenterY, 100)
hole.x = math.random(100,600)
hole.y = math.random(100,400)
hole:setFillColor(0,0,0)
hole.myName="target"
physics.addBody(hole, "static",{radius=100})
local ball


local onObjectTouch
local restartTimer

local function resolveColl( timerRef )
    timer.cancel( restartTimer )
    ball:removeEventListener( "collision" )
    transition.to(ball, {time=10, x=1000, y=10000})
end
local function over (Lives)
      if Lives == 0 then
        audio.stop()
        audio.play( gameoverSound )
        local displaypicture = display.newImage ("gameover.png", 1270, 1800)
        displaypicture.x=550
        displaypicture.y=970
      end
end

local function roofCollision()
  print( over (Lives))
  timer.cancel( restartTimer )
    ball:removeEventListener( "collision" )
    transition.to(ball, {time=10, x=1000, y=10000})
    audio.play( loselifeSound )
    Lives = Lives - 0.5
    LivesTxt.text = "Lives: " .. Lives
    if Lives <= 0 then
        Lives = 0
        physics.stop()
    LivesTxt.text = "Lives: " .. Lives
  print( over (Lives))
        
  
    end
end
local function sidewallcollision()
    print( over (Lives))
    timer.cancel( restartTimer )
    ball:removeEventListener( "collision" )
    transition.to(ball, {time=10, x=1000, y=10000})
    audio.play( loselifeSound )
    Lives = Lives - 0.5
    LivesTxt.text = "Lives: " .. Lives
    if Lives <= 0 then
        Lives = 0
        physics.stop()
    LivesTxt.text = "Lives: " .. Lives
    print( over (Lives)) 
   

    end
end    

roof:addEventListener("collision",roofCollision)
leftwall:addEventListener("collision",sidewallcollision)
rightwall:addEventListener("collision",sidewallcollision)

local function onLocalCollision( self, event )
    if ( event.phase == "began" ) then
      
  
      if(event.other.myName=="target") or event.other.myName=="roof" or event.other.myName=="leftwall" or event.other.myName=="rightwall"then
          local t = timer.performWithDelay( 10, resolveColl, 1 )
          audio.play( addlifeSound )
          Scores = Scores +1
          ScoresTxt.text = "Scores: " .. Scores
         

        
      


            
          
      end
    end
end
local function makeNewBall() 
  if(ball)then
    physics.removeBody( ball )
    ball:removeSelf( )
    ball:addEventListener( "touch", ball )
    ball:removeEventListener( "collision" ) 
  end  
   local NUM= math.random(1,4)
   if NUM == 1 then
     ball = display.newImage("apple.png",_W*0.5,_H*0.9)
   end
     if NUM == 2 then
       ball = display.newImage("Banana Icon.png",_W*0.5,_H*0.9)
     end
       if NUM == 3 then
         ball = display.newImage("orange.png",_W*0.5,_H*0.9)
       end
         if NUM == 4 then
           ball = display.newImage("Cocunut Icon.png",_W*0.5,_H*0.9)
         end
  ball.myName="ball"
  physics.addBody( ball,"dinamic", { density=0.1, friction=0.1, bounce=0.2, radius=60} )
  ball.touch = onObjectTouch
  ball:addEventListener( "touch", ball )
  ball.collision = onLocalCollision
  ball:addEventListener( "collision" )
       
end

local function enemyLive()
    restartTimer = timer.performWithDelay( 500, makeNewBall )
end
local function startAttack() 
    ball:removeEventListener( "touch", ball )
    transition.to( ball, { time=2000, width=ball.width*0.5, height=ball.height*0.5,onComplete=enemyLive } )
end
local startX, startY, endX, endY
onObjectTouch=function( self, event )
    if ( event.phase == "began" ) then
        startX,startY = event.x, event.y
        display.getCurrentStage():setFocus( self )
        self.isFocus = true
    elseif ( self.isFocus ) then
        if ( event.phase == "ended" or event.phase == "cancelled" ) then
            endX,endY = event.x, event.y
            local yAmount, xAmount = endY-startY, endX-startX
            display.getCurrentStage():setFocus( nil )
            self.isFocus = nil
            ball:setLinearVelocity( 1*xAmount, 1*yAmount )
            ball:applyTorque( xAmount )
            startAttack()
        end
    end
    return true
end


makeNewBall()

end